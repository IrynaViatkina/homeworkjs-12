let btn = document.getElementById('btn');

document.addEventListener('click', function(event){
	if(event.target.id === 'btn'){
		btn.remove();
		let diameter = document.createElement('div');
		let color = document.createElement('div');
		
		diameter.innerHTML = `<p>Введите диаметр круга</p><input id="diameter" placeholder="0"></input>`;
		color.innerHTML = `<p>Введите цвет круга</p><input id="color" placeholder="имя цвета, RGB, HEX, HSL"></input>`;
		
		document.body.appendChild(diameter);
		document.body.appendChild(color);
				
		let button = document.createElement('button');
		button.innerHTML = "Нарисовать";
		button.classList.add('btn');
		button.id = 'draw';
		document.body.appendChild(button);
	}
	else if(event.target.id === 'draw'){
		let circleDiameter = document.getElementById('diameter').value;
		let circleColor = document.getElementById('color').value;
		
		let circle = document.createElement('div');
		circle.style.width = `${circleDiameter}px`;
		circle.style.height = `${circleDiameter}px`;
		circle.style.borderRadius = `50%`;
		circle.style.backgroundColor = `${circleColor}`;
		document.body.appendChild(circle);
	}
});