let btn = document.getElementById('btn');

document.addEventListener('click', function(event){
	if(event.target.id === 'btn'){
		btn.remove();
		let diameter = document.createElement('div');		
		diameter.innerHTML = `<p>Введите диаметр круга</p><input id="diameter" placeholder="0"></input>`;
		document.body.appendChild(diameter);		
				
		let button = document.createElement('button');
		button.innerHTML = "Нарисовать";
		button.classList.add('btn');
		button.id = 'draw';
		document.body.appendChild(button);
	}
	else if(event.target.id === 'draw'){
		let circleDiameter = document.getElementById('diameter').value;
		drawCircles(circleDiameter);
	}
	else if(event.target.className === 'circle'){
		event.target.remove();
	}	
});

function drawCircles(diameter){
	let div;
	let circle;
	for(let i = 0; i < 10; i++){
		div = document.createElement('div');
		document.body.appendChild(div);
		for(let j = 0; j < 10; j++){
			circle = document.createElement('div');
			circle.classList.add('circle');
			
			circle.style.display = `inline-block`;
			circle.style.width = `${diameter}px`;
			circle.style.height = `${diameter}px`;
			circle.style.borderRadius = `50%`;
			circle.style.backgroundColor = `rgb(${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)})`;
			
			document.body.appendChild(circle);
		}
	}
}